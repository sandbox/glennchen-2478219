<?php

namespace Drupal\paste_your_snippet\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class ConfigForm extends ConfigFormBase {
  public function getFormId() {
    return 'paste_your_snippet_config';
  }

  public function getEditableConfigNames() {
    return ['paste_your_snippet.settings'];
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    drupal_set_message("buildForm called");
    $config = $this->config('paste_your_snippet.settings');

    $form['group'] = [
      '#type' => 'fieldset',
      '#title' => 'fieldset',
      //'#default_value' => $config->get('default_url'),
    ];

    $form['group']['url'] = [
      '#type' => 'textfield',
      '#title' => 'url',
      '#default_value' => $config->get('default_url'),
    ];

    $form['group']['snippet'] = [
          '#type' => 'textarea',
          '#title' => 'snippet',
          '#default_value' => $config->get('default_snippet'),
    ];

    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('paste_your_snippet.settings');
    drupal_set_message($config->get('default_link'));
    $config->set('default_url', $form_state->getValue('url'));
    $config->set('default_snippet', $form_state->getValue('snippet'));
    $config->save();
    parent::submitForm($form, $form_state);
  }
}